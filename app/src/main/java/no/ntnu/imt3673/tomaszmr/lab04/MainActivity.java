package no.ntnu.imt3673.tomaszmr.lab04;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * http://www.truiton.com/2015/06/android-tabs-example-fragments-viewpager/
 */
public class MainActivity
        extends AppCompatActivity
        implements UserFragment.OnUserSelectedListener,
            ChooseNicknameDialogFragment.OnNicknameChosenListener,
            SendMessageDialogFragment.OnSendListener {

    private static final String TAG = "AnonymousAuth";
    private static final String PREF_NICK = "username";
    private static final int CHECK_LATEST_MSG_JOB_ID = 12345;
    private static final int JOB_TIME_INTERVAL = 900000; // minimum 15 minutes
    private FirebaseAuth mAuth;
    private static String currentUser;

    private static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        fragmentManager = getSupportFragmentManager();

        FirebaseApp.initializeApp(this);
        this.mAuth = FirebaseAuth.getInstance();
        signInAnonymously();

        rescheduleMessageCheck();

        // start
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String username = readStringPreferences();
        if (username == null || username.equals("")) {
            showChooseNicknameDialog();
        } else {
            currentUser = username;
        }
        setupTabs();
        // end
    }

    private void showChooseNicknameDialog() {
        DialogFragment usernameDialog = new ChooseNicknameDialogFragment();
        usernameDialog.show(fragmentManager, "usernameDialog");
    }

    public static void showNewMessageDialog() {
        DialogFragment messageDialog = new SendMessageDialogFragment();
        messageDialog.show(fragmentManager, "messageDialog");
    }

    private void setupTabs() {
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("MSG"));
        tabLayout.addTab(tabLayout.newTab().setText("FRIENDS"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (fragmentManager, tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    /**
     * https://firebase.google.com/docs/auth/android/anonymous-auth
     */
    private void signInAnonymously() {
        // [START signin_anonymously]
        this.mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInAnonymously:success");
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInAnonymously:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        // [END signin_anonymously]
    }

    @Deprecated
    private void confirmFirebaseConnection(FirebaseUser currentUser) {
        if (currentUser != null ) {
            Toast toast = Toast.makeText(this, "Connected to firebase", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onUserSelected(String username) {
        System.out.println("............user " + username + " selected MainActivity");

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        assert tab != null;
        tab.setText(username);
        MessageFragment.fetchUserMessages(username);
    }

    @Override
    public void onNicknameChosen(String username) {
        currentUser = username;
        storePreferences(username);
        addUserToFirebase(username);
    }

    private void addUserToFirebase(String username) {
        Map<String, String> newUser = new HashMap<>();
        newUser.put("nickname", username);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .add(newUser)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                        updateUserList();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });
    }

    private void updateUserList() {
        // load new userlist
        UserFragment.fetchAllUsers();
    }

    private void storePreferences(String value) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(MainActivity.PREF_NICK, value);
        editor.apply();
    }

    private String readStringPreferences() {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        return prefs.getString(MainActivity.PREF_NICK, "");
    }

    @Override
    public void onMessageSend(String message) {
        System.out.println("............user: " + currentUser + "\nsends message: " + message);

        Map<String, Object> newMessage = new HashMap<>();
        newMessage.put("d", new Date());
        newMessage.put("u", currentUser);
        newMessage.put("m", message);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("messages")
                .add(newMessage)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                        updateMessageList();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });

    }

    private void updateMessageList() {
        MessageFragment.fetchAllMessages();
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        assert tab != null;
        tab.setText("MSG");
    }

    private void rescheduleMessageCheck() {
        JobScheduler jobScheduler =
                (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        assert jobScheduler != null;
        jobScheduler.schedule(new JobInfo.Builder(CHECK_LATEST_MSG_JOB_ID,
                new ComponentName(this, CheckNewMessagesService.class))
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPeriodic(MainActivity.JOB_TIME_INTERVAL)
                .build());
        System.out.println("....................rescheduleMessageCheck running...");
    }

}
