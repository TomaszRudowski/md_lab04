package no.ntnu.imt3673.tomaszmr.lab04;

import java.text.SimpleDateFormat;
import java.util.Date;

class Message {
    private final Date date;
    private final String sender;
    private final String content;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy  HH:mm");


    public Message(Date date, String sender, String content) {
        this.date = date;
        this.sender = sender;
        this.content = content;
    }


    // need only title to populate ListView, keeping default toString functionality of ArrayAdapter
    @Override
    public String toString() {
        return "(" + dateFormat.format(date) + ") " + sender + "\n> " + content;
    }
}
