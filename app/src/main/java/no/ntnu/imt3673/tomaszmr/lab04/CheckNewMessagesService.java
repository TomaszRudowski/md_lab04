package no.ntnu.imt3673.tomaszmr.lab04;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;


public class CheckNewMessagesService extends JobService {
    private static final String TAG_LAST_MSG = "Fetching last message";
    private static final String LAST_MSG_ID = "latest_message";

    private static final String CHANNEL_NAME = "new_msg_channel";
    private static final int CHANNEL_ID = 123;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        System.out.println(".................onStartJob running...");

        findLatestMessage();

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        System.out.println(".................onStopJob running...");
        return false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println(".........................onStartCommand running...");
        findLatestMessage();
        //return super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }


    private void findLatestMessage() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("messages")
                .orderBy("d", Query.Direction.DESCENDING)
                .limit(1)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG_LAST_MSG, document.getId() + " => " + document.getData());
                                //Date date = (Date)document.get("d");
                                String sender = (String)document.get("u");
                                //String content = (String)document.get("m");
                                foundMessage(document.getId(), sender);
                            }
                        } else {
                            Log.w(TAG_LAST_MSG, "Error getting documents.", task.getException());

                        }
                    }
                });
    }

    private void foundMessage(String messageID, String sender) {
        String lastNotified = readStringPreferences();
        System.out.println(".................foundMessage.." + messageID);
        if (!messageID.equals(lastNotified)) {
            storePreferences(messageID);
            System.out.println(".................newMessage replacing shared pref...need to send notification");
            createNotification(sender);
        } else {
            System.out.println(".................no replacing shared pref...no need to send notification");
        }
    }

    private void createNotification(String sender) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_NAME)
                    .setSmallIcon(R.drawable.ic_message_black_24dp)
                    .setContentTitle("New message from " + sender + " available")
                    .setContentText("Open app to read new message")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(true);

            // Create the NotificationChannel
            CharSequence name = "Channel Name";
            String description = "Channel Description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_NAME, name, importance);
            mChannel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = (NotificationManager) getSystemService(
                    NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mChannel);
                notificationManager.notify(CHANNEL_ID, mBuilder.build());
            }

        } else {
            // older systems
            Notification notification = new Notification.Builder(this)
                    .setContentTitle("New message from " + sender + " available")
                    .setSmallIcon(R.drawable.ic_message_black_24dp)
                    .setAutoCancel(true)
                    .build();
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            assert notificationManager != null;
            notificationManager.notify(0,notification);
        }
    }

    private String readStringPreferences() {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        return prefs.getString(CheckNewMessagesService.LAST_MSG_ID, "");
    }

    private void storePreferences(String value) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CheckNewMessagesService.LAST_MSG_ID, value);
        editor.apply();
    }
}
