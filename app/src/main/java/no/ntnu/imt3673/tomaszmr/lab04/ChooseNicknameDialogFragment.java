package no.ntnu.imt3673.tomaszmr.lab04;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.EditText;

import java.util.Random;

public class ChooseNicknameDialogFragment extends DialogFragment {

    private static final int MIN_NICKNAME_LENGTH = 5;
    private static final int MAX_NICKNAME_LENGTH = 8;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Choose nickname");

        final EditText inputField = new EditText(getContext());
        inputField.setText(createUniqueNickname());
        builder.setView(inputField);
                // Add action buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // store nickname ...
                        System.out.println(".......dialog. Ready to store username");
                        mListener.onNicknameChosen(inputField.getText().toString());
                    }
                });
        return builder.create();
    }

    /** based on
     * https://stackoverflow.com/questions/12116092/android-random-string-generator
     * @return random string starting with capital letter
     */
    private String createUniqueNickname() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        // gets string length in range MIN - MAX
        int randomLength = generator.nextInt(MAX_NICKNAME_LENGTH - MIN_NICKNAME_LENGTH + 1) + MIN_NICKNAME_LENGTH;
        char tempChar;
        tempChar = (char) (generator.nextInt(25) + 65);
        randomStringBuilder.append(tempChar);

        for (int i = 1; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(25) + 97);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    private ChooseNicknameDialogFragment.OnNicknameChosenListener mListener;

    // Container Activity must implement this interface
    public interface OnNicknameChosenListener {
        void onNicknameChosen(String username);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        if (context instanceof ChooseNicknameDialogFragment.OnNicknameChosenListener) {
            mListener = (ChooseNicknameDialogFragment.OnNicknameChosenListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
}
