
### lab 4 ###



### Lint warnings ###

* Lint: Correctness - (6 warn) - Kept older versions of Gradle dependencies, target API (26), and date preview (without Locale)

* Lint: Usability - (1 warn) - "App not indexable by Google Search" - skipping implementing support for Firebase App Indexing

* Code maturity issues - (1 warn)- used deprecated listener to work with the rest of copied code

* spelling - skipping



