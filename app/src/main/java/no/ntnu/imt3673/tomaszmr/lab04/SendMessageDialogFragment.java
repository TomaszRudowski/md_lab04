package no.ntnu.imt3673.tomaszmr.lab04;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.EditText;


public class SendMessageDialogFragment extends DialogFragment {

    private SendMessageDialogFragment.OnSendListener mListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Write a message");

        final EditText inputField = new EditText(getContext());
        builder.setView(inputField);
        // Add action buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                // store nickname ...
                System.out.println(".......dialog. Ready to send a message");
                mListener.onMessageSend(inputField.getText().toString());
            }
        });
        return builder.create();
    }

    public interface OnSendListener {
        void onMessageSend(String message);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        if (context instanceof SendMessageDialogFragment.OnSendListener) {
            mListener = (SendMessageDialogFragment.OnSendListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
}
