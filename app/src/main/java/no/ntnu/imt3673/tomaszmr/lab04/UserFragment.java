package no.ntnu.imt3673.tomaszmr.lab04;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;


public class UserFragment extends Fragment {
    private static final String TAG_USER = "Fetching users";
    private static List<String> users = new ArrayList<>();

    private String selectedUser;
    private static UserListViewAdapter userListViewAdapter;



    public UserFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("user " + selectedUser + " selected onCreateView");
        selectedUser = null;
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        System.out.println("user " + selectedUser + " selected onActivityCreated");
        userListViewAdapter = new UserListViewAdapter(getContext());

        final ListView usersListView = getActivity().findViewById(R.id.list_users);
        usersListView.setAdapter(userListViewAdapter);

        usersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                selectedUser = (String) usersListView.getItemAtPosition(pos);
                mListener.onUserSelected(selectedUser);
                //
                System.out.println("user " + selectedUser + " selected onItemClick...");
            }
        });


        fetchAllUsers();
    }

    public static void fetchAllUsers() {
        // reset list
        users = new ArrayList<>();
        // fetch all messages from Firebase
        //FirebaseFirestore db = MainActivity.getDatabase();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        // [START get_all_users]
        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG_USER, document.getId() + " => " + document.getData());
                                users.add((String)document.get("nickname"));
                            }
                            userListViewAdapter.updateListMessages(users);
                        } else {
                            Log.w(TAG_USER, "Error getting documents.", task.getException());
                        }
                    }
                });
        // [END get_all_users]
    }

    private OnUserSelectedListener mListener;

    // Container Activity must implement this interface
    public interface OnUserSelectedListener {
        void onUserSelected(String username);
    }
/*
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        if (context instanceof OnUserSelectedListener) {
            mListener = (OnUserSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
