package no.ntnu.imt3673.tomaszmr.lab04;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import java.util.List;

class UserListViewAdapter extends ArrayAdapter<String> {

    public UserListViewAdapter(@NonNull Context context) {
        super(context, android.R.layout.simple_list_item_1);
    }

    public void updateListMessages(List<String> users) {
        this.clear();
        this.addAll(users);
        notifyDataSetChanged();
    }
}
