package no.ntnu.imt3673.tomaszmr.lab04;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageFragment extends Fragment {

    private static final String TAG_MSG = "Fetching messages";
    private static List<Message> messages = new ArrayList<>();

    private static MessageListViewAdapter messageListViewAdapter;

    public MessageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("............onCreateView msgList.size=" + messages.size());
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FloatingActionButton fab = getActivity().findViewById(R.id.send_msg_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.showNewMessageDialog();
                /*
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        messageListViewAdapter = new MessageListViewAdapter(getContext());

        ListView messagesListView = getActivity().findViewById(R.id.list_messages);

        messagesListView.setAdapter(messageListViewAdapter);
        System.out.println("............onActivityCreated msgList.size=" + messages.size());
        fetchAllMessages();
    }


    public static void fetchAllMessages() {
        // reset list
        messages = new ArrayList<>();
        // fetch all messages from Firebase
        //FirebaseFirestore db = MainActivity.getDatabase();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        // [START get_all_users]
        db.collection("messages")
                .orderBy("d", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG_MSG, document.getId() + " => " + document.getData());
                                Date date = (Date)document.get("d");
                                String sender = (String)document.get("u");
                                String content = (String)document.get("m");
                                messages.add(new Message(date, sender, content));
                            }
                            messageListViewAdapter.updateListMessages(messages);
                        } else {
                            Log.w(TAG_MSG, "Error getting documents.", task.getException());
                            messageListViewAdapter.updateListMessages(messages);
                        }
                    }
                });
        // [END get_all_users]

    }

    public static void fetchUserMessages(String nickname) {
        // fetch all messages send by user with given nickname
        System.out.println("............fetchUserMessages, user selected: " + nickname);
        // reset list
        messages = new ArrayList<>();
        //messages.add(new Message(new Date(), nickname, "content"));
        //messageListViewAdapter.updateListMessages(messages);
        // fetch all messages from Firebase
        //FirebaseFirestore db = MainActivity.getDatabase();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        // [START get_all_users]
        db.collection("messages")
                .whereEqualTo("u", nickname)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG_MSG, document.getId() + " => " + document.getData());
                                Date date = (Date)document.get("d");
                                String sender = (String)document.get("u");
                                String content = (String)document.get("m");
                                messages.add(new Message(date, sender, content));
                            }
                            messageListViewAdapter.updateListMessages(messages);
                        } else {
                            Log.d(TAG_MSG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        // [END get_all_users]
    }
}
